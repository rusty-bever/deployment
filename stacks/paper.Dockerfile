ARG BASE_IMAGE

# We use ${:-} instead of a default value because the argument is always passed
# to the build, it'll just be blank most likely
FROM ${BASE_IMAGE:-'openjdk:17-slim'}

# Build arguments
ARG MC_VERSION
ARG PAPERMC_VERSION

# Install mcstatus for healthchecks
RUN apt update && \
    apt install --no-install-recommends -y python3-minimal python3-setuptools python3-pip && \
    python3 -m pip install mcstatus && \
    apt --purge remove -y python3-pip python3-setuptools && \
    apt clean

# Create worlds and config directory
WORKDIR /app
RUN mkdir worlds config

# Download server file
ADD "https://papermc.io/api/v2/projects/paper/versions/$MC_VERSION/builds/$PAPERMC_VERSION/downloads/paper-$MC_VERSION-$PAPERMC_VERSION.jar" server.jar

# Store the cache in an anonymous volume, which means it won't get stored in the other volumes
VOLUME /app/config/cache


# Default value to keep users from eating up all ram accidentally
ENV XMX=4
ENV MC_VERSION="${MC_VERSION}"
ENV PAPERMC_VERSION="${PAPERMC_VERSION}"

# Document exposed ports
EXPOSE 25565

# Entrypoint runs in /app/config
COPY entrypoint.sh /entrypoint.sh
WORKDIR /app/config
ENTRYPOINT /entrypoint.sh

HEALTHCHECK --interval=30s --timeout=5s --start-period=15s --retries=3 \
    CMD mcstatus localhost:25565 ping
