# Build arguments
The only required build argument is `RELEASE_TAG`. This is the GitHub tag of
the release you wish to use. The releases can be found
[here](https://github.com/Pryaxis/TShock/releases). The release tag is the
identifier on the left. It should look something like  `v4.4.0-pre15`. You can
then specify the variable in the `.env` file: e.g. `RELEASE_TAG=v4.4.0-pre15`

# Environment variables
The only provided environment variable is `AUTOCREATE`. This should be either
`1`, `2` or `3`. It specifies how big the generated world should, e.g. small,
medium or large. If a world file already exists, the existing file is used
instead of generating a new one.

# Mount points
There a three useful mount points defined:

* `/terraria/config`: this is where the server config files are located
* `/terraria/logs`: the server log files are dumped here
* `/terraria/worlds`: this is where the world files are stored

You can mount these directories somewhere in the host file system by specifying
the mount paths in the `.env` file. These can be both absolute or relative
paths.

# Other config variables
You can specify the port using the `PORT` variable. This configures which port
on the host system should be exposed for the Terraria server. This is also the
port you need to enter into the Terraria client in order to connect to the
server. The default port for Terraria servers is `7777`.
