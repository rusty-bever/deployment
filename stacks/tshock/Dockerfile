FROM alpine AS base

# Build arguments
ARG RELEASE_TAG
ARG DI_VER=1.2.5

WORKDIR /app

# Build dumb-init & download tshock
RUN apk add --update --no-cache build-base unzip curl && \
    curl -Lo - "https://github.com/Yelp/dumb-init/archive/refs/tags/v${DI_VER}.tar.gz" | tar -xzf - && \
    cd "dumb-init-${DI_VER}" && \
    make SHELL=/bin/sh && \
    mv dumb-init .. && \
    cd .. && \
    curl -s "https://api.github.com/repos/Pryaxis/TShock/releases/tags/${RELEASE_TAG}" | \
        grep "browser_download_url" | \
        grep -o "https[^\"]\+" | \
    xargs curl -sLo tshock.zip && \
    unzip -d tshock tshock.zip && \
    rm tshock.zip


FROM mono:6.12.0.182

RUN useradd -m terraria && \
    mkdir /app && \
    chown -R terraria /app

USER terraria

WORKDIR /app

COPY --from=base /app/tshock /app/dumb-init /app/

# Create worlds directory & symlink it
RUN mkdir -p worlds logs config "$HOME/.local/share/Terraria" && \
    ln -s /app/worlds "$HOME/.local/share/Terraria/Worlds"

ENTRYPOINT ["/app/dumb-init", "--"]
CMD mono /app/TerrariaServer.exe \
    -configpath /app/config \
    -logpath /app/logs \
    -autocreate "$AUTOCREATE" \
    -world /app/worlds/Main.wld
