# Ansible Debian

This repository contains all the Ansible playbooks & roles I use to manage my various Debian-based servers & Docker swarms.

## Playbooks

* [`bootstrap.yml`](bootstrap.yml): handles first-time configuration of a server. This is the only playbook that expects you should run as your root user. It creates a non-root user that can use sudo, & secures the SSH configuration. Afterwards, you should edit your hosts file to reflect the changes.
* [`update.yml`](update.yml): update your servers. When a new Debian version is released, the sources.list file can be changed. Afterwards, your servers will automatically update to this new version when this playbook is ran.
* [`main.yml`](main.yml): This is the playbook that you can run multiple times without issues. It sets up the Docker swarm, network settings, etc.
